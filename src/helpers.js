export function debounce(actionFunction, timeoutMs) {
  let currentTimerId = null;
  return (...args) => {
    if (currentTimerId) {
      clearTimeout(currentTimerId);
    }

    currentTimerId = setTimeout(() => {
      currentTimerId = null;
      actionFunction(...args);
    }, timeoutMs);
  };
}
