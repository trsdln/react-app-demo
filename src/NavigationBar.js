import { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { STORAGE_TOKEN_NAME, BACKEND_URL } from "./constants.js";

const getNavLinkClassName = ({ isActive, isPending }) =>
  isPending ? "pending" : isActive ? "active" : "";

export function NavigationBar() {
  const [isLoggedIn, setIsLoggedIn] = useState(
    !!localStorage.getItem(STORAGE_TOKEN_NAME)
  );

  const [currentUser, setCurrentUser] = useState({});

  useEffect(() => {
    async function fetchCurrentUser() {
      const res = await fetch(`${BACKEND_URL}/current-user`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem(STORAGE_TOKEN_NAME)}`,
        },
      });
      const userData = await res.json();
      setCurrentUser(userData);
    }
    if (isLoggedIn) {
      fetchCurrentUser();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onLogout = () => {
    localStorage.removeItem(STORAGE_TOKEN_NAME);
    setIsLoggedIn(false);
  };

  return (
    <span className="navigation-bar">
      <NavLink to="/" className={getNavLinkClassName}>
        Home
      </NavLink>
      {isLoggedIn && (
        <NavLink to="/contacts" className={getNavLinkClassName}>
          Contacts
        </NavLink>
      )}
      {!isLoggedIn && (
        <>
          <NavLink to="/login" className={getNavLinkClassName}>
            Login
          </NavLink>
          <NavLink to="/sign-up" className={getNavLinkClassName}>
            Sign Up
          </NavLink>
        </>
      )}
      {isLoggedIn && (
        <>
          <span className="login-status">
            Logged in as: {currentUser.email}
          </span>
          <button onClick={onLogout}>Log Out</button>
        </>
      )}
    </span>
  );
}
