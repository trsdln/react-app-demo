import { createBrowserRouter, Outlet, useOutlet } from "react-router-dom";

import { HomePage } from "./pages/Home.js";
import { SignUpPage } from "./pages/SignUpPage.js";
import { ContactsPage } from "./pages/ContactsPage/ContactsPage.js";
import { NavigationBar } from "./NavigationBar.js";
import { LoginPage } from "./pages/LoginPage.js";

function RootLayout() {
  const outletElement = useOutlet();
  return (
    <>
      <NavigationBar />
      {outletElement ? <Outlet /> : <HomePage />}
    </>
  );
}

export const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    children: [
      {
        path: "sign-up",
        element: <SignUpPage />,
      },
      {
        path: "contacts",
        element: <ContactsPage />,
      },
      {
        path: "login",
        element: <LoginPage />,
      },
    ],
  },
]);
