export function ContactEntry({ user }) {
  return (
    <div className="user-entry">
      <span className="user-name">
        {user.first_name} {user.last_name}
      </span>
      <span className="user-email">{user.email}</span>
    </div>
  );
}
