import { useState, useEffect, useCallback, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { debounce } from "../../helpers.js";
import { BACKEND_URL, STORAGE_TOKEN_NAME } from "../../constants.js";
import { ContactEntry } from "./ContactEntry.js";

export function ContactsPage() {
  const navigate = useNavigate();
  const searchInputRef = useRef(null);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState("");
  const [users, setUsers] = useState([]);

  const loadUsers = async (event) => {
    setIsLoading(true);
    try {
      const { value } = event.target;

      const searchParams = new URLSearchParams();
      if (value) {
        searchParams.set("phrase", value);
      }
      const res = await fetch(
        `${BACKEND_URL}/contacts?${searchParams.toString()}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(STORAGE_TOKEN_NAME)}`,
          },
        }
      );

      if (res.status === 403) {
        navigate("/login");
        return;
      }

      const users = await res.json();
      setIsLoading(false);
      setUsers(users);
    } catch (err) {
      setError(err.toString());
    }
  };

  useEffect(() => {
    loadUsers({ target: { value: "" } });
    searchInputRef.current.focus();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const onSearchChange = useCallback(debounce(loadUsers, 300), []);

  return (
    <div>
      <h1>Current Users List</h1>

      <input
        ref={searchInputRef}
        placeholder="start typing to search"
        className="search-input"
        onChange={onSearchChange}
      />

      {isLoading && <div>Loading...</div>}
      {!isLoading && error && <div>Error: {this.state.error}</div>}
      {!isLoading &&
        users.map((user) => <ContactEntry key={user._id} user={user} />)}
    </div>
  );
}
