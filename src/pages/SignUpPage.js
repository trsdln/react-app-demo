import { useNavigate } from "react-router-dom";
import { BACKEND_URL, STORAGE_TOKEN_NAME } from "../constants.js";
import { UserForm } from "../components/UserForm.js";

export function SignUpPage() {
  const navigate = useNavigate();

  const onSubmit = async (userData, setError) => {
    const res = await fetch(`${BACKEND_URL}/signup`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(userData),
    });

    const data = await res.json();

    if (res.status >= 400) {
      setError(data.error);
    } else {
      localStorage.setItem(STORAGE_TOKEN_NAME, data.token);
      navigate("/contacts");
    }
  };

  return (
    <UserForm
      title="Please, Sign Up"
      onSubmit={onSubmit}
      submitCaption="Sign Up"
    />
  );
}
