import { useRef, useState } from "react";
import { string, func } from "prop-types";

export function UserForm({ title, onSubmit, submitCaption }) {
  const emailInput = useRef(null);
  const passwordInput = useRef(null);
  const [error, setError] = useState("");

  const internalOnSubmit = async () => {
    setError("");

    const userData = {
      email: emailInput.current.value,
      password: passwordInput.current.value,
    };

    await onSubmit(userData, setError);
  };

  return (
    <div className="user-form">
      <h3>{title}</h3>
      <input
        type="text"
        className="form-email"
        placeholder="test@email.com"
        ref={emailInput}
      />
      <input type="password" placeholder="type password" ref={passwordInput} />

      <div className="submit-error">{error}</div>

      <button className="submit-button" onClick={internalOnSubmit}>
        {submitCaption}
      </button>
    </div>
  );
}

UserForm.propTypes = {
  title: string.isRequired,
  onSubmit: func.isRequired,
  submitCaption: string.isRequired,
};
